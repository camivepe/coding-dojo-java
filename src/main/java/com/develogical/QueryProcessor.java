package com.develogical;

import java.util.StringTokenizer;

public class QueryProcessor {

    public String process(String query) {
        System.out.println("----QUERY: "+query); 
    	if (query.contains("hi")) {
            return "hello";
        }
        if (query.contains("name")) {
            return "MoniCami";
        }
        if (query.contains("largest")) {
        	String cortada = query.substring(query.indexOf(":")+1).trim();
        	System.out.println("cortada: "+cortada);
        	StringTokenizer tokens=new StringTokenizer(cortada, ",");
        	System.out.println("numeros: "+tokens.countTokens());
        	Integer numero1 = null;
        	Integer respuesta = null;
        	Integer numero2 = null;
        	while(tokens.hasMoreTokens()){
        		if(numero1 == null){
        			numero1 = new Integer( tokens.nextToken() );
                	System.out.println("numero1: "+numero1);
                	respuesta = numero1;
        		}else{
	        		        			
        			numero2 = new Integer( tokens.nextToken().replace("," , "").trim());
	        		System.out.println("numero2: "+numero2);
	                if(respuesta < numero2){
	                	respuesta = numero2;	                	
	                }
        		}
            }
        	System.out.println("respuesta: "+respuesta);
        	return ""+respuesta;
        }
        if (query.contains("plus")) {
        	String cortada = query.substring(query.indexOf("is")+2).trim();
        	System.out.println("cortada: "+cortada);
        	
        	String n1 = cortada.substring(0, cortada.indexOf("plus")).trim();
        	System.out.println("n1: "+n1);
        	Integer numero1 = new Integer(n1);
        	Integer respuesta = null;
        	String n2 = cortada.substring(cortada.indexOf("plus")+4, cortada.length()).trim();
        	System.out.println("n2: "+n2);
        	Integer numero2 = new Integer(n2);
        	
        	respuesta = numero1 + numero2;
        	System.out.println("respuesta: "+respuesta);
        	return ""+respuesta;
        }
        
        if (query.contains("multiplied")) {
        	String cortada = query.substring(query.indexOf("is")+2).trim();
        	System.out.println("cortada: "+cortada);
        	
        	String n1 = cortada.substring(0, cortada.indexOf("multiplied by")).trim();
        	System.out.println("n1: "+n1);
        	Integer numero1 = new Integer(n1);
        	Integer respuesta = null;
        	String n2 = cortada.substring(cortada.indexOf("multiplied by")+"multiplied by".length(), cortada.length()).trim();
        	System.out.println("n2: "+n2);
        	Integer numero2 = new Integer(n2);
        	
        	respuesta = numero1 * numero2;
        	System.out.println("respuesta: "+respuesta);
        	return ""+respuesta;
        }
        
        return "";
    }

}
