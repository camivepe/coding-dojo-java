package com.develogical;

import org.junit.Test;

import java.util.Map;

import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.containsString;

public class QueryProcessorTest {

    @Test
    public void canGreetYou() {
        String result = new QueryProcessor().process("hi");
        assertNotNull(result);
        assertThat(result, is("hello"));
    }

    @Test
    public void returnsEmptyStringForUnknownQueries() {
        String result = new QueryProcessor().process("unknown");
        assertNotNull(result);
        assertThat(result, is(""));
    }
    
    @Test
    public void returnsEmptyStringForNameQueries() {
        String result = new QueryProcessor().process("what is your name");
        assertNotNull(result);
        assertThat(result, is("MoniCami"));
    }
    
    
    @Test
    public void returnsEmptyStringForLargestsQueries() {
    	String pregunta = "largest: 100, 20, 5";    	
        String result = new QueryProcessor().process(pregunta);
        assertNotNull(result);
        assertThat(result, is("100"));
    }
    
    @Test
    public void returnsEmptyStringForSumaQueries() {
    	String pregunta = "is 33 plus 22";    
    	//%20what%20is%2011%20plus%203 
    	String result = new QueryProcessor().process(pregunta);
        assertNotNull(result);
        assertThat(result, is("55"));
    }
    
    @Test
    public void returnsEmptyStringForMultiQueries() {
    	String pregunta = "is 5 multiplied by 3";    
    	
    	String result = new QueryProcessor().process(pregunta);
        assertNotNull(result);
        assertThat(result, is("15"));
    }
}



